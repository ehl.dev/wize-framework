import HelloWorld from "./components/HelloWorld.vue";
import UserDropdown from "./components/header/user-dropdown/UserDropdown.vue";

const WizeFramework = {
 install(Vue) {
  Vue.component("WizeHelloWorld", HelloWorld);
  Vue.component("UserDropdown", UserDropdown);
 }
};

// Automatic installation if Vue has been added to the global scope.
if (typeof window !== 'undefined' && window.Vue) {
    window.Vue.use(WizeFramework);
}

export default WizeFramework;